# The Things Network: SX1301a REF Board based gateway for use in the US

Reference setup for [The Things Network](http://thethingsnetwork.org/) gateways based on the SX1301a

## Setup the pin out for the reference board
- Connect the 5V pin of the RPi to the center pin of J200 male breakout pins
- Connect a ground pin on the Rpi to the closest pin to the edge of the PCB of CONN200 male breakout pins (they are on the bottom, 3 of them)
- Connect the SCK pin of the RPi to pin 1 of the J400 male Breakout
- Connect pin 7 of the Rpi to pin 3 of the J400 male Breakout
- Connect the MISO pin of the RPi to pin 5 of the J400 male breakout
- Connect the CS_0 pin of the RPi to pin 7 of the J400 male breakout
- Connect the MOSI pin of the RPi to pin 9 on the J400 male breakout

## Setup based on Raspbian image

- Download [Raspbian Lite](https://www.raspberrypi.org/downloads/)
- Follow the [installation instruction](https://www.raspberrypi.org/documentation/installation/installing-images/README.md) to create the SD card
- Start your RPi with a keyboard, mouse, and monitor connected to it
- Default password of a plain-vanilla RASPBIAN install for user `pi` is `raspberry`
- Use `raspi-config` utility to expand the filesystem, connect to CaseGuest, and enable SPI:

        $ sudo raspi-config

- Reboot
- Configure locales and time zone:

        $ sudo dpkg-reconfigure locales
        $ sudo dpkg-reconfigure tzdata

- Make sure you have an updated installation and install `git`:

        $ sudo apt-get update
        $ sudo apt-get upgrade
        $ sudo apt-get install git

- Create new user for TTN and add it to sudoers

        $ sudo adduser ttn
        $ sudo adduser ttn sudo

- To prevent the system asking root password regularly, add TTN user in sudoers file

        $ sudo visudo

- Logout and login as `ttn` and remove the default `pi` user

        $ sudo userdel -rf pi

- Copy the contents of the `Gateway` directory onto the RPi and start the installation

        $ cd ~/gateway
        $ sudo chmod a+x ./install.sh # might need to make file executable
        $ sudo ./install.sh

- **Big Success!** You should now have a running gateway in front of you!

## Setup gateway in ttn

The best way to check if the gateway is working is registering it on the TTN Console.

- Login to thethingsnetwork.org `Console`
- Click on `Gateways -> register gateway`
- Enable checkbox `I'm using the legacy packet forwarder`
- Enter your Gateway EUI (if is printed on start and end of the installer and can also be found on the Rpi at `/opt/ttn-gateway/.bin/local_conf.json`)
- Enter any description
- Select United States 915Mhz as frequency plan
- Confirm clicking Register gateway

Now you can see the status (which at this point should be connected if we did everything right) and traffic of your gateway! (give it a sec)
